@extends('layouts.theme')

@section('title')
    Welcome to Blog
@endsection

@section('content')
    @parent
    @foreach ($articles as $article)
    <section class="post-section">
        <header>
            <h1 class="post-title"><a href="/article/{{ $article->id }}">{{ $article->title }}</a></h1>
            <h6><a href="/category/{{ $article->category->id }}">{{ $article->category->name }}</a></h6>
        </header>
        <section class="post-content">
            <p>{{ $article->content }}</p>
        </section>
    </section>
    @endforeach
@endsection
