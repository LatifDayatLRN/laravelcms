<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link href="{{ asset('css/stylesheet.css') }}" rel="stylesheet">
</head>
<body>
    <header class="header-c p-a p-b-t">
        <h1>Laravel CMS Demo</h1>
        <nav class="nav-top a-bc-red a-br-s">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/article">Blog</a></li>
            </ul>
        </nav>
    </header>
    <main class="p-a p-b-t" >
        @section('content')
            <p>Halaman Artikel</p>
        @show

    </main>

</body>
</html>
