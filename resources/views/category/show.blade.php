@extends('layouts.theme')

@section('title')
    Welcome to Blog
@endsection

@section('content')
    @foreach( $category->articles as $article  )
    <h1><a href="/article/{{ $article->id }}">{{ $article->title }}</a></h1>
    <h6><a href="/category/{{ $article->category->id }}">{{ $article->category->name }}</a></h6>
    <section>
        <p>{{ $article->content }}</p>
    </section>
    @endforeach
@endsection
