@extends('layouts.theme')

@section('title')
    Welcome to Blog
@endsection

@section('content')
    @foreach ($categories as $category)
    <h1>{{ $category->name }}</h1>
    @endforeach
@endsection
