<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;

class ArticleController extends Controller
{
    public function show( Request $request ) {
        $article = Article::find($request->id)->firstOrFail();
    }

    public function add ( Request $request ) {
        $article = new Article;
        $article->title = $request->title;
        $article->content = $request->content;
        $article->category = Category::where('name', $requset->category )->firstOrFail()->id;
        $article->save();
    }

    public function update ( Request $request ) {
        $article = App\Article::findOrFail($request->id);
        $article->title = $request->title;
        $article->content = $request->content;
        $article->category = Category::where('name', $requset->category )->firstOrFail()->id;
    }

    public function delete ( Request $request ) {
        $article = App\Article::find($request->id);
        $article->delete();
    }
}
